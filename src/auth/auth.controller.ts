import { Controller, Get, Post, Param, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { User } from './user.entity';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Post('/signup')
    signUp(@Body() authCredentialsDTO: AuthCredentialsDTO) {
        return this.authService.signUp(authCredentialsDTO);
    }

    // @Get()
    // findAll() {
    //     return this.authService.getAll();
    // };

    // @Get('/:id')
    // async findUser(@Param('id') id: number): Promise<User> {
    //     return await this.authService.getUser(id);
    // };

    // @Post()
    // async createUser(@Body() createUserDTO: User): Promise<User> {
    //     return this.authService.createUser(createUserDTO);
    // }


}

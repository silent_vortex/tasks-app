import { Repository } from "typeorm";
import { User } from "./user.entity";
import { AuthCredentialsDTO } from "./dto/auth-credentials.dto";

// @EntityRepository(User)

export interface UserRepository extends Repository<User> {
    // this: Repository<User>;
    async createUser(authCredentialsDTO: AuthCredentialsDTO): Promise<void> {
    const { userName, userPassword } = authCredentialsDTO;
}
    // getUsers(): Promise<User[]>;
    // getUser(id: number): Promise<User>;
    // createUser(user: { userId: string; userName: string; userPassword: string; })
}



import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UserRepository } from './users.repository';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User) private readonly userRepository: UserRepository
    ) { }

    // async getAll() {
    //     return await this.userRepository.getUsers();
    // };

    // async getUser(id: number): Promise<User> {
    //     return await this.userRepository.getUser(id);
    // }

    // async createUser(userDTO): Promise<User> {
    //     const user: User = this.userRepository.create();
    //     user.userName = 'John';
    //     user.userPassword = '12345abdef';

    //     return await this.userRepository.createUser(user);
    // };


}
